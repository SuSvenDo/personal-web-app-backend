package de.dataenv.root.contact.job

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import de.dataenv.root.contact.job.rest.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.security.oauth2.jwt.JwtDecoder
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status


const val CERN_JOB_DISC = "Head of Software Department."

@SpringBootTest(
    properties = [
        "spring.cloud.kubernetes.enabled=false",
        "spring.jpa.show-sql=true",
        "spring.jpa.properties.hibernate.format_sql=true",
        "spring.mail.username=username@mail.com",
        "spring.mail.receiver=receiver@mail.com"]
)
@DirtiesContext
@AutoConfigureMockMvc
class TestListJobOffers(@Autowired val mockMvc: MockMvc, @Autowired val offerRepo: OfferRepo) {

    @MockBean
    lateinit var jwtDecoder: JwtDecoder

    @MockBean
    lateinit var emailSender: JavaMailSender

    @Test
    @WithMockUser
    fun when_list_of_jobs_is_requested_without_job_offers_scope_access_is_denied() {
        mockMvc.perform(get("/api/contact/offer")).andExpect(status().isForbidden())
    }

    @Test
    fun when_list_of_jobs_is_requested_without_user_scope_access_is_denied() {
        mockMvc.perform(get("/api/contact/offer")).andExpect(status().isUnauthorized())
    }

    @Test
    @WithMockUser(authorities = ["admin"])
    fun list_job_offers() {
        offerRepo.save(
            OfferEntity(
                JobEntity("CEO", "Best Job Ever"),
                EmployerEntity("CERN", "Cool"),
                ContactEntity("Stephen", "Hawking", "steve@mail.com", "0123456789")
            )
        );
        val result = mockMvc.perform(get("/api/contact/offer")).andReturn();
        val response = jacksonObjectMapper().readValue<OfferListResponse>(result.response.contentAsString);
        assertEquals(1, response.offers.get(0).id)
        assertEquals("CEO", response.offers.get(0).job.title)
        assertEquals("Best Job Ever", response.offers.get(0).job.description)
        assertEquals("CERN", response.offers.get(0).employer.name)
        assertEquals("Cool", response.offers.get(0).employer.description)
        assertEquals("Stephen", response.offers.get(0).contact.firstName)
        assertEquals("Hawking", response.offers.get(0).contact.lastName)
        assertEquals("steve@mail.com", response.offers.get(0).contact.email)
        assertEquals("0123456789", response.offers.get(0).contact.telephone)
    }
}