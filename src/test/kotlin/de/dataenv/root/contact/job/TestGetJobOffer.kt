package de.dataenv.root.contact.job

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import de.dataenv.root.contact.job.rest.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.security.oauth2.jwt.JwtDecoder
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status



@SpringBootTest(
    properties = [
        "spring.cloud.kubernetes.enabled=false",
        "spring.jpa.show-sql=true",
        "spring.jpa.properties.hibernate.format_sql=true",
        "spring.mail.username=username@mail.com",
        "spring.mail.receiver=receiver@mail.com"]
)
@DirtiesContext
@AutoConfigureMockMvc
class TestGetJobOffer(@Autowired val mockMvc: MockMvc, @Autowired val offerRepo: OfferRepo) {

    @MockBean
    lateinit var jwtDecoder: JwtDecoder

    @MockBean
    lateinit var emailSender: JavaMailSender

    @Test
    @WithMockUser
    fun when_a_job_is_requested_without_job_offers_scope_access_is_denied() {
        mockMvc.perform(get("/api/contact/offer/1")).andExpect(status().isForbidden())
    }

    @Test
    fun when_a_job_is_requested_without_user_scope_access_is_denied() {
        mockMvc.perform(get("/api/contact/offer/1")).andExpect(status().isUnauthorized())
    }

    @Test
    @WithMockUser(authorities = ["admin"])
    fun get_job_offer() {
        offerRepo.save(
            OfferEntity(
                JobEntity("CEO", "Best Job Ever"),
                EmployerEntity("CERN", "Cool"),
                ContactEntity("Stephen", "Hawking", "steve@mail.com", "0123456789")
            )
        );
        val result = mockMvc.perform(get("/api/contact/offer/1")).andReturn();
        val response = jacksonObjectMapper().readValue<OfferResponse>(result.response.contentAsString);
        assertEquals(1, response.id)
        assertEquals("CEO", response.job.title)
        assertEquals("Best Job Ever", response.job.description)
        assertEquals("CERN", response.employer.name)
        assertEquals("Cool", response.employer.description)
        assertEquals("Stephen", response.contact.firstName)
        assertEquals("Hawking", response.contact.lastName)
        assertEquals("steve@mail.com", response.contact.email)
        assertEquals("0123456789", response.contact.telephone)
    }
}