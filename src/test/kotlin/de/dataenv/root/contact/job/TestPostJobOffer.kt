package de.dataenv.root.contact.job

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import de.dataenv.root.contact.job.rest.*
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.mockito.ArgumentCaptor
import org.mockito.Captor
import org.mockito.Mockito.verify
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.mail.SimpleMailMessage
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.security.oauth2.jwt.JwtDecoder
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status


@SpringBootTest(
    properties = [
        "spring.cloud.kubernetes.enabled=false",
        "spring.jpa.show-sql=true",
        "spring.jpa.properties.hibernate.format_sql=true",
        "spring.mail.username=username@mail.com",
        "spring.mail.receiver=receiver@mail.com"
    ]
)
@DirtiesContext
@AutoConfigureMockMvc
class TestPostJobOffer(@Autowired val mockMvc: MockMvc, @Autowired val offerRepo: OfferRepo) {

    @MockBean
    lateinit var jwtDecoder: JwtDecoder

    @MockBean
    lateinit var emailSender: JavaMailSender

    @Captor
    lateinit var mailCaptor: ArgumentCaptor<SimpleMailMessage>

    var offer = OfferRequest(
        JobRequest("CEO", "Best Job Ever"),
        EmployerRequest("CERN", "Cool"),
        ContactRequest("Stephen", "Hawking", "steve@mail.com", "0123456789")
    )

    @Test
    @WithMockUser
    fun when_offer_is_posted_without_auth_status_is_ok() {
        var post = post("/api/contact/offer")
            .contentType(MediaType.APPLICATION_JSON)
            .content(jacksonObjectMapper().writeValueAsString(offer));
        mockMvc.perform(post).andExpect(status().isOk())
        verify(emailSender).send(mailCaptor.capture())
        assertEquals("username@mail.com" , mailCaptor.value.from);
    }

}