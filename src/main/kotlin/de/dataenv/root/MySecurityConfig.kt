package de.dataenv.root

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.config.Customizer
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter
import org.springframework.security.web.SecurityFilterChain


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
class MySecurityConfig {

    @Bean
    open fun filterChain(http: HttpSecurity): SecurityFilterChain {
        return http.csrf().disable()
            .httpBasic().disable()
            .authorizeRequests(Customizer { authz ->
                authz.anyRequest().permitAll()
            }).oauth2ResourceServer { oauth2 ->
                oauth2.jwt()
                    .jwtAuthenticationConverter(jwtAuthenticationConverter());
            }.build()
    }

    private fun jwtAuthenticationConverter(): JwtAuthenticationConverter? {
        val jwtAuthenticationConverter = JwtAuthenticationConverter()
        jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter { jwt -> getAuthorities(jwt) }
        return jwtAuthenticationConverter
    }

    fun getAuthorities(jwt: Jwt): List<GrantedAuthority> {
        var realmAccess = jwt.claims.get("realm_access")
        var map = jacksonObjectMapper().readValue<Map<String, List<String>>>(realmAccess.toString())
        return map.get("roles")?.map { role -> SimpleGrantedAuthority(role) } ?: listOf();
    }

}