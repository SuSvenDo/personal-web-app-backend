package de.dataenv.root.contact.job.rest

class OfferResponse(
    var id: Int?,
    var job: JobResponse,
    var employer: EmployerResponse,
    var contact: ContactResponse,
) {
    constructor(entity: OfferEntity) : this(
        entity.id,
        JobResponse(entity.job),
        EmployerResponse(entity.employer),
        ContactResponse(entity.contact)
    )
}

class JobResponse(
    var title: String,
    var description: String
) {
    constructor(entity: JobEntity) : this(entity.title, entity.description)
}

class EmployerResponse(
    var name: String,
    var description: String
) {
    constructor(entity: EmployerEntity) : this(entity.name, entity.description)
}

class ContactResponse(
    var firstName: String,
    var lastName: String,
    var email: String,
    var telephone: String
) {
    constructor(entity: ContactEntity) : this(entity.firstName, entity.lastName, entity.email, entity.telephone)
}

class OfferListResponse(var offers: List<OfferResponse>) {
}