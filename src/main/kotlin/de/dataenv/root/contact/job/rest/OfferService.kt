package de.dataenv.root.contact.job.rest

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.mail.SimpleMailMessage
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.stereotype.Service


@Service
class OfferService(@Autowired val offerRepo: OfferRepo, @Autowired val emailSender: JavaMailSender) {

    @Value("\${spring.mail.username}")
    private lateinit var username: String
    @Value("\${spring.mail.receiver}")
    private lateinit var receiver: String

    fun save(job: OfferEntity): OfferEntity {
        var job = offerRepo.save(job);
        val message = SimpleMailMessage()
        message.setFrom(this.username)
        message.setTo(this.receiver)
        message.setSubject("Job Offer: " + job.job.title)
        message.setText("empty")
        emailSender!!.send(message)
        return job;
    }

    fun find(): List<OfferResponse> {
        return offerRepo.findAll().map { jobEntity -> OfferResponse(jobEntity) };
    }

    fun findById(id: Integer): OfferEntity {
        return offerRepo.findById(id).orElseThrow();
    }
}