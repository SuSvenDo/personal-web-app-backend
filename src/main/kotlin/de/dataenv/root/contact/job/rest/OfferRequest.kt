package de.dataenv.root.contact.job.rest

class OfferRequest(
    var job: JobRequest,
    var employer: EmployerRequest,
    var contact: ContactRequest,
) {}

class JobRequest(
    var title: String,
    var description: String
) {}

class EmployerRequest(
    var name: String,
    var description: String
) {}

class ContactRequest(
    var firstName: String,
    var lastName: String,
    var email: String,
    var telephone: String
){}