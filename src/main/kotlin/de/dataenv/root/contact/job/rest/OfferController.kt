package de.dataenv.root.contact.job.rest

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*


@RestController
@RequestMapping("/api/contact/offer")
class OfferController(@Autowired val offerService: OfferService) {

    @GetMapping("{id}")
    @PreAuthorize("hasAuthority('admin')")
    fun getOffer(@PathVariable id: Integer): OfferResponse {
        return OfferResponse(offerService.findById(id))
    }

    @GetMapping
    @PreAuthorize("hasAuthority('admin')")
    fun getOffers(): OfferListResponse {
        return OfferListResponse(offerService.find());
    }

    @PostMapping()
    fun postOffer(@RequestBody projectRequest: OfferRequest): OfferResponse {
        var offerEntity = OfferEntity(projectRequest);
        offerEntity = offerService.save(offerEntity)
        return OfferResponse(offerEntity);
    }

}