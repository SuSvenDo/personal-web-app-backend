package de.dataenv.root.contact.job.rest

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import javax.persistence.*

@Repository
interface OfferRepo : CrudRepository<OfferEntity, Integer> {}

@Entity
class OfferEntity(

    @OneToOne(cascade = [CascadeType.ALL])
    var job: JobEntity,

    @OneToOne(cascade = [CascadeType.ALL])
    var employer: EmployerEntity,

    @OneToOne(cascade = [CascadeType.ALL])
    var contact: ContactEntity,

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Int? = null
) {
    constructor(request: OfferRequest) : this(
        JobEntity(request.job), EmployerEntity(request.employer), ContactEntity(request.contact)
    )
}

@Entity
class JobEntity(
    var title: String,
    var description: String,
    @OneToOne(mappedBy = "job")
    var offer: OfferEntity? = null,
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Int? = null
) {
    constructor(jobRequest: JobRequest) : this(jobRequest.title, jobRequest.description)
}

@Entity
class EmployerEntity(
    var name: String,
    var description: String,
    @OneToOne(mappedBy = "employer")
    var offer: OfferEntity? = null,
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Int? = null
) {
    constructor(employerRequest: EmployerRequest) : this(
        employerRequest.name,
        employerRequest.description
    )
}


@Entity
class ContactEntity(
    var firstName: String,
    var lastName: String,
    var email: String,
    var telephone: String,
    @OneToOne(mappedBy = "employer")
    var offer: OfferEntity? = null,
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Int? = null
) {
    constructor(request: ContactRequest) : this(
        request.firstName,
        request.lastName,
        request.email,
        request.telephone
    )
}