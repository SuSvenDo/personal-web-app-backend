package de.dataenv.root

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.client.discovery.EnableDiscoveryClient

@SpringBootApplication
open class Main

fun main(args: Array<String>) {
    SpringApplication.run(Main::class.java, *args)
}